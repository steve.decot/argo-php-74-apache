# argo-php-74-apache
## Description 
IAC pour le deploy de l'application dont le repos est ici : https://gitlab.com/steve.decot/k8s-web-php
L'image docker est ici : https://hub.docker.com/r/stedec/webphp74

### Résumé de l'application php :

* Code php qui retourne l'ip du node sur lequel le code est runné. 
* Php7.4 + apache sous debian buster - php:7.4-apache-buster  - https://hub.docker.com/layers/library/php/7.4-apache-buster/images/sha256-873b11069a2725565daad54b2c2167507b2d46c40337e94381029196cf34ea65?context=explore
* User koba créé sans droit particulier
* Designé pour tourner sous Openshift mais aussi sur Docker
* curl 
* Rootless

## GitOps
Désigné pour tourner avec ArgoCD via les kustomizations files. 

## Author 
Steve Decot 
